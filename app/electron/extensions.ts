// Quick react, redux and devtron extensions installer
// The installation promise must be resolved before creating BrowserWindow
// The extensions are not permanently enabled because they throw non-fatal errors
export async function installExtensions() {
  const installer = require('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = [
    'REACT_DEVELOPER_TOOLS',
    'REDUX_DEVTOOLS',
    'DEVTRON'
  ];

  return Promise
    .all(extensions.map(name => installer.default(installer[name], forceDownload)))
    .catch(console.log);
}
