import React, { useState, useLayoutEffect } from 'react';
import styled from 'styled-components';
import * as firebase from 'firebase/app';
import { useHistory } from 'react-router-dom';

import SectionTitle from 'components/SectionTitle';
import Input from 'components/Input';
import Button from 'components/Button';
import LinkText from 'components/LinkText';
import ErrorMessage from 'components/ErrorMessage';
import Loading from 'components/Loading';

import useKeyPress from 'hooks/useKeyPress';

const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Form = styled.div`
  margin-bottom: 50px;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const InputWrapper = styled.div`
  margin-bottom: 5px;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

const StyledErrorMessage = styled(ErrorMessage)`
  width: 100%;
  text-align: center;
`;

const SignInButton = styled(Button)`
  margin: 15px 0 8px;
`;

function SignIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();

  async function onSignInClick() {
    if (!email) {
      setError('Email is required.');
      return;
    }

    if (!password) {
      setError('Password is required.');
      return;
    }

    setIsLoading(true);
    setError('');

    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
      history.push('/');
    } catch (e) {
      setError(e.message);
    }

    setIsLoading(false);
  }

  const isEnterPressed = useKeyPress('Enter');

  useLayoutEffect(() => {
    if (isEnterPressed) {
      onSignInClick();
    }
  }, [onSignInClick, isEnterPressed]);

  return (
    <Container>
      Sign in to Sidekick
      <Form>
        <InputWrapper>
          <SectionTitle>
            EMAIL
          </SectionTitle>
          <Input
            type="email"
            value={email}
            placeholder="Your email address"
            onChange={(e: any) => setEmail(e.target.value)}
          />
        </InputWrapper>

        <InputWrapper>
          <SectionTitle>
            PASSWORD
          </SectionTitle>
          <Input
            type="password"
            value={password}
            placeholder="Your password"
            onChange={(e: any) => setPassword(e.target.value)}
          />
        </InputWrapper>
      </Form>

      {error && (
        <StyledErrorMessage>
          {error}
        </StyledErrorMessage>
      )}

      {isLoading && <Loading text="" />}
      {!isLoading && (
        <>
          <SignInButton
            onClick={onSignInClick}
          >
            SIGN IN
          </SignInButton>

          <LinkText
            to="/sign-up"
          >
            Sign up instead
          </LinkText>
        </>
      )}
    </Container>
  );
}

export default SignIn;

