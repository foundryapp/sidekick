import React, { useState, useLayoutEffect } from 'react';
import styled from 'styled-components';
import * as firebase from 'firebase/app';
import axios from 'axios';
import { useHistory } from 'react-router-dom';

import SectionTitle from 'components/SectionTitle';
import Input from 'components/Input';
import Button from 'components/Button';
import LinkText from 'components/LinkText';
import ErrorMessage from 'components/ErrorMessage';
import Loading from 'components/Loading';
import useKeyPress from 'hooks/useKeyPress';

const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Form = styled.div`
  margin-bottom: 50px;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const InputWrapper = styled.div`
  margin-bottom: 5px;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`;

const StyledErrorMessage = styled(ErrorMessage)`
  width: 100%;
  text-align: center;
`;

const SignUpButton = styled(Button)`
  margin: 15px 0 8px;
`;

function SignUp() {
  const [teamName, setTeamName] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();

  async function onSignUpClick() {
    setError('');
    setIsLoading(true);

    if (!teamName) {
      setError('Team name is required.');
      setIsLoading(false);
      return;
    }

    if (!username) {
      setError('Username is required.');
      setIsLoading(false);
      return;
    }

    if (!email) {
      setError('Email is required.');
      setIsLoading(false);
      return;
    }

    if (!password) {
      setError('Password is required.');
      setIsLoading(false);
      return;
    }

    try {
      const isTeamAvailablePromise = axios.post('https://us-central1-terminal-sidekick.cloudfunctions.net/isTeamNameAvailable', { teamName });
      const isUsernameAvailablePromise = axios.post('https://us-central1-terminal-sidekick.cloudfunctions.net/isUsernameAvailable', { username });

      const [isTeamAvailableResponse, isUsernameAvailableResponse] = await Promise.all([isTeamAvailablePromise, isUsernameAvailablePromise]);

      const isTeamNameAvailable: boolean = isTeamAvailableResponse.data.isAvailable as boolean;
      if (!isTeamNameAvailable) {
        setError('Team name is already taken.');
        setIsLoading(false);
        return
      }

      const isUsernameAvailable: boolean = isUsernameAvailableResponse.data.isAvailable as boolean;
      if (!isUsernameAvailable) {
        setError('Username is already taken.');
        setIsLoading(false);
        return;
      }

    } catch (e) {
      console.error(e.response.data || e.message);
      setError(e.response.data || e.message);
      setIsLoading(false);
      return;
    }

    let userID = '';
    try {
      const { user } = await firebase.auth().createUserWithEmailAndPassword(email, password);
      if (user) {
        userID = user.uid;
      } else {
        throw new Error('Failed to create user (no user)');
      }
    } catch (e) {
      console.error(e.message);
      setError(e.message);
      setIsLoading(false);
      return;
    }

    try {
      await axios.post('https://us-central1-terminal-sidekick.cloudfunctions.net/createUserWithTeamInDB', { username, teamName, userID });
      // Notify that user is authenticated only after we are sure that a default team has been created
    } catch (e) {
      // TODO: User has been created but the team doc and user doc isn't in the DB
      console.error(e.response.data || e.message);
      setError(e.response.data || e.message);
      setIsLoading(false);
      return;
    }

    setIsLoading(false);
    history.replace('/home');
  }

  const isEnterPressed = useKeyPress('Enter');

  useLayoutEffect(() => {
    if (isEnterPressed) {
      onSignUpClick();
    }
  }, [onSignUpClick, isEnterPressed]);

  return (
    <Container>
      Sign up to Sidekick
      <Form>
        <InputWrapper>
          <SectionTitle>
            TEAM NAME
          </SectionTitle>
          <Input
            value={teamName}
            placeholder="Name of your team"
            onChange={(e: any) => setTeamName(e.target.value)}
          />
        </InputWrapper>

        <InputWrapper>
          <SectionTitle>
            USERNAME
          </SectionTitle>
          <Input
            value={username}
            placeholder="Your username"
            onChange={(e: any) => setUsername(e.target.value)}
          />
        </InputWrapper>

        <InputWrapper>
          <SectionTitle>
            EMAIL
          </SectionTitle>
          <Input
            type="email"
            value={email}
            placeholder="Your email address"
            onChange={(e: any) => setEmail(e.target.value)}
          />
        </InputWrapper>

        <InputWrapper>
          <SectionTitle>
            PASSWORD
          </SectionTitle>
          <Input
            type="password"
            value={password}
            placeholder="Your password"
            onChange={(e: any) => setPassword(e.target.value)}
          />
        </InputWrapper>
      </Form>

      {error && (
        <StyledErrorMessage>
          {error}
        </StyledErrorMessage>
      )}

      {isLoading && <Loading text="" />}
      {!isLoading && (
        <>
          <SignUpButton
            onClick={onSignUpClick}
          >
            SIGN UP
          </SignUpButton>

          <LinkText
            to="/sign-in"
          >
            Sign in instead
          </LinkText>
        </>
      )}
    </Container>
  );
}

export default SignUp;

