import React from 'react';
import ReactDOM from 'react-dom';
import * as firebase from 'firebase/app';

import { notifyViewReady } from './mainProcess';

import './index.css';
import firebaseInit from './Firebase';
import App from './App';
import * as serviceWorker from './serviceWorker';


firebaseInit();

firebase.auth().onAuthStateChanged(user => {

  // TODO: Get database user and team info


  // TODO: We are re-rendering the whole app again
  ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    document.getElementById('root')
  );
});

notifyViewReady();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
