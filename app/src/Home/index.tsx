import React, {
  useEffect,
  useMemo,
  useState
} from 'react';
import styled from 'styled-components';
import * as firebase from 'firebase/app';
// import { useCollection } from 'react-firebase-hooks/firestore';
import electron, {
  notifyUserAuthenticated,
  requestPendingSessionInvites,
  rejectShellSession,
  acceptShellSession,
  shareShellSession,
  requestShellSessions,
  turnTerminalHighlightOff,
  changeTeam,
} from 'mainProcess';
import SectionTitle from 'components/SectionTitle';
import Loading from 'components/Loading';
import Button from 'components/Button';
import Title from 'components/Title';
import Input from 'components/Input';

import { ReactComponent as ChevronDownIcon } from 'img/chevron-down.svg';
import axios from 'axios';
import { TerminalEntry } from '@foundryapp/sidekick-terminal-server';
import TeamMember, { Member } from './TeamMember';
import SelectTeamModal from './SelectTeamModal';
import SessionInvite from './SessionInvite';
import InviteLinkModal from './InviteLinkModal';
import SelectTerminalModal from './SelectTerminalModal';
import Commands from './Commands';
import CommandModal from './Commands/CommandModal';
import { CommandInfo } from './Commands/Command';


const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const TeamWrapper = styled.div`
  display: flex;
  align-content: center;
`;

const TeamTitle = styled(Title)`
  margin: 5px 0 10px;
`;

const SelectTeamButton = styled.div`
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content: center;
  margin: 5px 8px;
  :hover {
    cursor: pointer;
  }
`;

const TeamMembers = styled.div`
  display: flex;
  flex-direction: column;
`;

const InviteLinkButton = styled.button`
  margin: 15px 0 5px;
  padding: 10px;
  color: #fff;
  font-size: 16px;
  font-weight: 600;
  width: 100%;

  background: #5C4FED;
  border-radius: 5px;
  border: none;
  outline: none;
  :hover {
    cursor: pointer;
  }
`;

const SectionTitleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const AddCommandButton = styled.div`
  font-size: 12px;
  font-weight: 600;
  color: #5C4FED;

  :hover {
    cursor: pointer;
  }
`;

function Home() {
  const [isLoading, setIsLoading] = useState(true);
  const [onlineTeamMembers, setOnlineTeamMembers] = useState<Member[]>([]);
  /*
  const [userTeams, loadingTeams, errorTeams] = useCollection(
    firebase.firestore().collection('teams').where('members', 'array-contains', firebase.auth().currentUser?.uid),
  );
  */
  const [userTeams, setUserTeams] = useState<string[]>([]);
  const [activeTeamID, setActiveTeamID] = useState('');
  const [isSelectTeamModalOpened, setIsSelectTeamModalOpened] = useState(false);
  const [sessionInviteRequest, setSessionInviteRequest] = useState<any>(null);
  const [isTerminalModalOpened, setIsTerminalModalOpened] = useState(false);
  const [isCommandModalOpened, setIsCommandModalOpened] = useState(false);
  const [localShellSessions, setLocalShellSessions] = useState<TerminalEntry[]>([]);
  const [teamModalError, setTeamModalError] = useState('');
  const [isTeamModalLoading, setIsTeamModalLoading] = useState(false);
  const [search, setSearch] = useState('');

  const [editedCommand, setEditedCommand] = useState<CommandInfo>();

  const [isInviteLinkModalOpened, setIsInviteLinkModalOpened] = useState(false);
  const [currentUser, setCurrentUser] = useState<firebase.User>();

  const username = onlineTeamMembers.find((member) => {
    return member.userID === currentUser?.uid;
  })?.username;

  function setTeam(teamName: string) {
    changeTeam(teamName);
    setActiveTeamID(teamName);
  }

  useEffect(() => {
    async function getUserTeamsAndNotify(userID: string) {
      const query = await firebase.firestore().collection('teams').where('members', 'array-contains', userID).get();
      const ts = query.docs.map(d => d.id).sort();
      if (ts.length === 0) {
        // TODO: This shouldn't happen because each user should have at least 1 team that is created during sign up
      }
      setUserTeams(ts);

      const active = ts[0];
      setTeam(active);
    }

    async function getCurrentLocalShellSessions() {
      const sessions = await requestShellSessions();
      setLocalShellSessions(sessions);
    }

    // WARNING:!!!
    // TODO: NEEDS FIX!!!
    // All there electron.ipcRenderer.on(...) are closures so if they are using
    // some local state it will get captured during the initialization and it
    // won't have the up-to-date value!!!!

    // New users online
    electron.ipcRenderer.on('update-presence', (event, { members }: { members: Array<any> }) => {
      console.log('update-presence [renderer]', members);

      setOnlineTeamMembers(members);
      setIsLoading(false);
    });
    // Terminal session invite.
    electron.ipcRenderer.on('session-invite', (event, {
      user,
      sessionID,
      hostShellSession
    }) => {
      console.log('session-invite [renderer] (user, sessionID, hostShellSession)', user, sessionID, hostShellSession);
      setSessionInviteRequest({ user, sessionID, hostShellSession });
    });
    // Local shell session opened.
    electron.ipcRenderer.on('local-shell-session-opened', (event, shellSession: TerminalEntry) => {
      console.log('Local shell session opened', shellSession);
      setLocalShellSessions(c => c.concat([shellSession]));
    });
    // Local shell session closed.
    electron.ipcRenderer.on('local-shell-session-closed', (event, shellSession: TerminalEntry) => {
      console.log('Local shell session closed', shellSession);
      setLocalShellSessions(c => c.filter(s => s.terminalId !== shellSession.terminalId));
    });
    // Connection error to the WS server.
    electron.ipcRenderer.on('connect_error', (event, { error }) => {
      console.log('connect error to the WS server:', error);
    });
    // Connnection timeout to the WS server.
    electron.ipcRenderer.on('connect_timeout', (event, { timeout }) => {
      console.log('connect timeout for the WS server', timeout);
    });
    // Disconnected from the WS server.
    electron.ipcRenderer.on('disconnect', (event) => {
      console.log('disconnected from the WS server');
    });
    // Session invite got invalidated by server.
    electron.ipcRenderer.on('session-hangout', (event, { hostID, sessionID }) => {
      console.log('session hangout [renderer] (hostID, sessionID):', hostID, sessionID);
      setSessionInviteRequest(null);
    });

    const user = firebase.auth().currentUser;
    if (user) {
      getUserTeamsAndNotify(user.uid);
    } else {
      // TODO: Go to /?
    }

    // Get info about shells that might be already running
    getCurrentLocalShellSessions();

  }, []);

  useEffect(() => {
    async function getPendingInvites() {
      const invs = await requestPendingSessionInvites();
      if (invs.length > 0) {
        // TODO: Can't handle more than 1 session invite
        setSessionInviteRequest(invs[0]);
      }
    }

    if (!isLoading) {
      getPendingInvites();
    }
  }, [isLoading]);

  useEffect(() => {
    const user = firebase.auth().currentUser;
    if (user) {
      setIsLoading(true);
      // We are notifying the main process here because we want to notify it about
      // user auth only once we are sure that a default team for the user has already
      // been created.
      if (activeTeamID) {
        notifyUserAuthenticated(user.uid, activeTeamID);
      }
      setCurrentUser(user);
    }
  }, [activeTeamID]);

  function handleSelectTeam(teamID: string) {
    setTeam(teamID);
    setIsSelectTeamModalOpened(false);
  }

  function closeSelectTeamModal() {
    setIsSelectTeamModalOpened(false);
  }

  function openSelectTeamModal() {
    setIsSelectTeamModalOpened(true);
  }

  function openCommandModal(command?: CommandInfo) {
    setEditedCommand(command);
    setIsCommandModalOpened(true);
  }

  function closeCommandModal() {
    setIsCommandModalOpened(false);
  }

  async function handleJoinTeam(teamID: string) {
    console.log('Want to join team:', teamID);
    setTeamModalError('');
    setIsTeamModalLoading(true);

    const user = firebase.auth().currentUser;
    try {
      // TODO: Handle this better
      if (!user) {
        throw new Error('Something went wrong. No current user.');
      }
      await axios.post('https://us-central1-terminal-sidekick.cloudfunctions.net/joinTeam', { teamID, userID: user.uid });
      setUserTeams(c => c.concat([teamID]));
      setIsSelectTeamModalOpened(false);
    } catch (e) {
      setTeamModalError(e.response.data || e.message);
    }

    setIsTeamModalLoading(false);
  }

  /*
  function openTerminalModal() {
    setIsTerminalModalOpened(true);
  }

  function closeTerminalModal() {
    localShellSessions.forEach(s => turnTerminalHighlightOff(s.terminalId));
    setIsTerminalModalOpened(false);
  }
  */

  function handleSessionInviteAccept() {
    acceptShellSession(sessionInviteRequest.sessionID, sessionInviteRequest.hostShellSession);
  }

  function handleSessionInviteReject() {
    rejectShellSession(sessionInviteRequest.sessionID);
    setSessionInviteRequest(null);
  }

  function handleSessionInviteFinishedLoading() {
    setSessionInviteRequest(null);
  }

  /*
  function handleSelectTerminalForSession(terminalID: string) {
    acceptShellSession(sessionInviteRequest.sessionID, terminalID);
    setSessionInviteRequest(null);
    closeTerminalModal();
  }
  */

  async function handleShareSessionSelect(targetUserID: string, session: TerminalEntry) {
    console.log('Share shell session to user:', targetUserID);
    await shareShellSession(targetUserID, session);
  }

  return (
    <Container>
      {isLoading && <Loading text="" />}
      {!isLoading && (
        <>
          {isSelectTeamModalOpened && (
            <SelectTeamModal
              onCloseModalRequest={closeSelectTeamModal}
              onSelectTeam={handleSelectTeam}
              teamIDs={userTeams}
              onJoinTeam={handleJoinTeam}
              error={teamModalError}
              isLoading={isTeamModalLoading}
            />
          )}

          {/*
          {isTerminalModalOpened && (
            <SelectTerminalModal
              onCloseModalRequest={closeTerminalModal}
              onSelectTerminal={handleSelectTerminalForSession}
              shellSessions={localShellSessions}
            />
          )}
          */}

          {isInviteLinkModalOpened && activeTeamID && username && currentUser?.email &&
            <InviteLinkModal
              teamName={activeTeamID}
              username={username}
              email={currentUser.email}
              onCloseModalRequest={() => setIsInviteLinkModalOpened(false)}
            />
          }

          {isCommandModalOpened && activeTeamID &&
            <CommandModal
              command={editedCommand}
              teamID={activeTeamID}
              onCloseModalRequest={() => setIsCommandModalOpened(false)}
            />
          }

          <TeamWrapper>
            <TeamTitle>
              {activeTeamID}
            </TeamTitle>
            <SelectTeamButton
              onClick={openSelectTeamModal}
            >
              <ChevronDownIcon />
            </SelectTeamButton>
          </TeamWrapper>

          {/* <SectionTitle>
            HISTORY (CMD-F)
          </SectionTitle>
          <Input
            placeholder={"Search your team history"}
            value={search}
            onChange={e => setSearch(e.target.value)}
          /> */}

          <SectionTitleWrapper>
            <SectionTitle>
              TEAM COMMANDS
            </SectionTitle>
            <AddCommandButton
              onClick={() => openCommandModal()}
            >
              Add command
            </AddCommandButton>
          </SectionTitleWrapper>
          <Commands
            openCommandModal={openCommandModal}
            teamID={activeTeamID}
          />
          <SectionTitle>
            TEAM
          </SectionTitle>
          {sessionInviteRequest && (
            <SessionInvite
              hostUsername={sessionInviteRequest.user.username}
              onAccept={handleSessionInviteAccept}
              onReject={handleSessionInviteReject}
              onFinishedLoading={handleSessionInviteFinishedLoading}
            />
          )}
          <TeamMembers>
            {[
              ...onlineTeamMembers.filter(user => user.userID === currentUser?.uid),
              ...onlineTeamMembers.filter(user => user.userID !== currentUser?.uid && user.isOnline),
              ...onlineTeamMembers.filter(user => user.userID !== currentUser?.uid && !user.isOnline),
            ].map(m => (
              <TeamMember
                key={m.userID}
                member={m}
                shellSessions={localShellSessions}
                onShareSessionSelect={handleShareSessionSelect}
              />
            ))}
          </TeamMembers>
          <InviteLinkButton onClick={() => setIsInviteLinkModalOpened(true)}>Invite Team Member</InviteLinkButton>
        </>
      )
      }
    </Container >
  );
}

export default Home;

