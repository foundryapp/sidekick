import React, { useState } from 'react';
import styled from 'styled-components';

import firebase from 'firebase/app';

import Modal from 'components/Modal';
import Loading from 'components/Loading';
import ErrorMessage from 'components/ErrorMessage';
import Input from 'components/Input';

import { CommandInfo } from '../Command';

const Container = styled(Modal)`
  /* width: 250px; */
  width: 95%;
  height: 70%;
  display: flex;
  padding: 10px;
  margin: 0px;
`;


const ControlBar = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 8px;
`;


const CommandNameArea = styled(Input)`
  font-weight: 500;
  font-size: 15px;
  width: 100%;
  margin: 0px;
  padding: 5px 5px;
  margin-right: 5px;

  border: 1px solid #5C4FED;
`;

const CommandValueArea = styled.textarea`
  flex: 1;
  font-family: 'Source Code Pro';
  width: 100%;
  padding: 10px;
  font-size: 14px;
  border-radius: 3px;
  color: white;

  border: none;
  white-space: nowrap;
  overflow: scroll;
  background: #24272E;

  /*
  I couldn't find a way to both disable the resize corner and style the corner -
  this workaroud keeps the vertical resizer that does nothing and styles the resizer background.
  */
  ::-webkit-resizer {
    background: #24272E;
  }
  resize: vertical;
`;

const SaveButton = styled.div<{ isActive: boolean }>`
  font-size: 14px;
  font-weight: 600;
  padding: 5px;

  color: ${({ isActive }: { isActive: boolean }) => isActive ? 'white' : '#FBFBFB'};

  :hover {
    cursor: ${({ isActive }: { isActive: boolean }) => isActive ? 'pointer' : 'default'};
  }
`;

const DeleteButton = styled.div`
  font-size: 14px;
  font-weight: 600;
  padding: 5px;
  :hover {
    cursor: pointer;
  }
`;

interface EditCommandModalProps {
  command?: CommandInfo;
  teamID: string;
  onCloseModalRequest: () => void;
}

async function deleteCommandFromDB(command: CommandInfo, teamID: string) {
  console.log(`Deleting command ${command.name} in team ${teamID}`);
  try {
    await firebase.firestore().collection('teams').doc(teamID).collection('commands').doc(command.id).delete();
  } catch (error) {
    console.error(error);
    throw new Error('Could not delete command from the DB');
  }
}

async function saveCommandEditsToDB(command: CommandInfo | undefined, name: string, value: string, teamID: string) {
  if (name === '' || name === undefined) {
    throw new Error('Command name must not be empty');
  }
  if (value === '' || value === undefined) {
    throw new Error('Command must not be empty');
  }
  console.log(`Updating command ${name} with value ${value} in team ${teamID}`);
  try {
    if (command) {
      await firebase.firestore().collection('teams').doc(teamID).collection('commands').doc(command?.id).update({ name, value });
    } else {
      await firebase.firestore().collection('teams').doc(teamID).collection('commands').add({ name, value });
    }
  } catch (error) {
    console.error(error);
    throw new Error('Could not save command to the DB');
  }
}

function CommandModal(props: EditCommandModalProps) {
  const [isLoading, setIsLoading] = useState(false);

  const [name, setName] = useState(props.command ? props.command.name : '');
  const [value, setValue] = useState(props.command ? props.command.value : '');
  const [error, setError] = useState('');

  async function deleteCommand() {
    if (props.command) {
      setIsLoading(true);
      try {
        await deleteCommandFromDB(props.command, props.teamID);
      } catch (error) {
        setError(error);
        return;
      } finally {
        setIsLoading(false);
      }
      props.onCloseModalRequest();
    }
  }

  async function editCommand() {
    if (!name || !value) {
      return;
    }
    setIsLoading(true);
    try {
      await saveCommandEditsToDB(props?.command, name, value, props.teamID);
    } catch (error) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
    props.onCloseModalRequest();
  }

  return (
    <Container onCloseModalRequest={props.onCloseModalRequest}>
      {isLoading && <Loading text='' />}
      {!isLoading &&
        <>
          <ControlBar>
            <CommandNameArea
              value={name}
              placeholder='Command name'
              onChange={e => setName(e.target.value)}
            />
            {props.command &&
              <DeleteButton
                onClick={deleteCommand}
              >
                Delete
              </DeleteButton>
            }
            <SaveButton
              onClick={editCommand}
              isActive={!!name && !!value}
            >
              Save
            </SaveButton>
          </ControlBar>
          <CommandValueArea
            value={value}
            placeholder='Type command or script'
            onChange={e => setValue(e.target.value)}
          />
          {error &&
            <ErrorMessage>
              {error}
            </ErrorMessage>}
        </>
      }
    </Container>
  );
}


export default CommandModal;
