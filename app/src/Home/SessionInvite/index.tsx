import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import electron from 'mainProcess';

const Container = styled.div`
  margin: 5px 0 15px;
  padding: 5px 10px;
  width: 100%;
  display: flex;
  flex-direction: column;
  border-radius: 3px;
  background: #42444A;
`;

const Text = styled.span`
  width: 100%;
  font-size: 14px;
  font-weight: 400;
  color: white;
`;

const Username = styled(Text)`
  font-weight: 600;
`;

const Buttons = styled.div`
  margin-top: 10px;
  display: flex;
`;

const ResponseButton = styled.button`
  padding: 5px 10px;
  font-size: 15px;
  font-weight: 600;
  border-radius: 3px;
  border: none;
  outline: none;
  :hover {
    cursor: pointer;
  }
`;

const AcceptButton = styled(ResponseButton)`
  margin-right: 10px;
  color: #25C93A;
  background: rgba(37, 201, 58, 0.15);
`;

const RejectButton = styled(ResponseButton)`
  color: #FF4C45;
  background: rgba(255, 76, 69, 0.15);
`;

const Info = styled.div`
  margin-top: 10px;
  width: 100%;
  text-align: left;
  color: #A0A1A4;
  font-size: 15px;
  font-weight: 500;
`;

interface SessionInviteProps {
  hostUsername: string;
  onAccept: () => void;
  onReject: () => void;
  onFinishedLoading: () => void;
}

function SessionInvite(props: SessionInviteProps) {
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    electron.ipcRenderer.on('host-shell-session-for-sharing-created', () => {
      console.log('host-shell-session-for-sharing-created [renderer]');
      setIsLoading(false);
      props.onFinishedLoading();
    });
  }, []);

  function onAccept() {
    setIsLoading(true);
    props.onAccept();
  }

  return (
    <Container>
      <Text><Username>{props.hostUsername}</Username> wants to share terminal with you</Text>
      {!isLoading && (
        <Buttons>
          <AcceptButton
            onClick={onAccept}
          >Accept
          </AcceptButton>

          <RejectButton
            onClick={props.onReject}
          >Reject
          </RejectButton>
        </Buttons>
      )}
      {isLoading && <Info>Loading session, please wait...</Info>}
    </Container>
  );
}

export default SessionInvite;

