import React, { useEffect } from 'react';
import styled from 'styled-components';
import { TerminalEntry } from '@foundryapp/sidekick-terminal-server';
import Modal from 'components/Modal';
import { Member } from 'Home/TeamMember';

import { turnTerminalHighlightOn } from 'mainProcess';

const Container = styled(Modal)`
  width: 250px;
`;

const Title = styled.span`
  margin-bottom: 15px;
  font-size: 16px;
  font-weight: 500;
  color: #fff;
  text-align: center;
`;

const Info = styled.span`
  font-size: 14px;
  font-weight: 500;
  color: #A0A1A4;
  text-align: center;
`;


const SelectSessionLink = styled.span`
  max-width: 220px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  margin-bottom: 8px;
  font-size: 14px;
  font-weight: 500;
  color: #4FA9ED;
  :hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;

interface ShellSessionModalProps {
  onCloseModalRequest: () => void;
  member: Member;
  shellSessions: TerminalEntry[];
  onShareSessionSelect: (session: TerminalEntry) => void;
}

function ShellSessionModal(props: ShellSessionModalProps) {

  useEffect(() => {
    props.shellSessions.forEach(s => turnTerminalHighlightOn(s.terminalId, `${s.terminalIndex}`));
  }, [props.shellSessions]);

  return (
    <Container
      onCloseModalRequest={props.onCloseModalRequest}
    >
      <Title>Select shell session to share</Title>
      {props.shellSessions.length === 0 && (
        <Info>
          No terminal session registered.
          Try opening a new terminal window.
        </Info>
      )}

      {props.shellSessions.map(s => (
        <SelectSessionLink
          key={s.terminalId}
          onClick={() => props.onShareSessionSelect(s)}
        >Terminal {s.terminalIndex}
        </SelectSessionLink>
      ))}
    </Container>
  );
}

export default ShellSessionModal;
