import React from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import * as firebase from 'firebase/app';
import Modal from 'components/Modal';
import Button from 'components/Button';

import { disconnect } from 'mainProcess';


const Container = styled(Modal)`
`;

interface UserSettingsModalProps {
  onCloseModalRequest: () => void;
}

function UserSettingsModal(props: UserSettingsModalProps) {
  const history = useHistory();

  async function handleSignOutClick() {
    await firebase.auth().signOut();
    await disconnect();
    history.push('/');
  }

  return (
    <Container
      onCloseModalRequest={props.onCloseModalRequest}
    >
      <Button
        onClick={handleSignOutClick}
      >Sign Out
      </Button>
    </Container>
  );
}

export default UserSettingsModal;

