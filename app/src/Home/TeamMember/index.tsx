import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import * as firebase from 'firebase/app';
import { TerminalEntry } from '@foundryapp/sidekick-terminal-server';
import { ReactComponent as DotsIcon } from 'img/dots.svg';
import { turnTerminalHighlightOff } from 'mainProcess';

import UserSettingsModal from './UserSettingsModal';
import ShareShellSessionModal from './ShareShellSessionModal';


const Container = styled.div`
  margin-bottom: 3px;
  padding: 0px 5px 0px 10px;
  width: 100%;
  min-height: 34px;
  display: flex;
  align-items: center;

  /* background: #42444A; */
  /* border-radius: 3px; */
`;

const ActivityDot = styled.div<{ isOnline: boolean }>`
  width: 10px;
  margin: 5px 15px 5px 0px;
  height: 10px;
  background: ${(props) => props.isOnline && '#25C93A'};
  border: ${(props) => !props.isOnline && ' 1px solid #6E7075'};

  border-radius: 30px;
`;

const Username = styled.div<{ isOnline: boolean }>`
  color: ${(props) => props.isOnline ? '#FFFFFF' : '#6E7075'};
  font-weight: 500;
  font-size: 14px;
  margin-bottom: 1px;
  align-items: center;
`;

const YouTag = styled.span`
  margin-left: 5px;
  color: #6E7075;
  font-weight: 400;
  font-size: 14px;
`;

const RightSideWrapper = styled.div`
  display: flex;
  flex: 1;
  justify-content: flex-end;
  align-items: center;
`;

const ShareTerminalButton = styled.button`
  padding: 5px 8px;

  color: #FFFFFF;
  font-size: 12px;
  font-weight: 600;

  background: #5C4FED;
  border-radius: 3px;
  border: none;
  outline: none;
  :hover {
    cursor: pointer;
  }
`;

const UserSettings = styled(DotsIcon)`
  :hover {
    cursor: pointer;
  }
`;

export interface Member {
  userID: string;
  username: string;
  isOnline: boolean;
}

interface TeamMemberProps {
  member: Member;
  shellSessions: TerminalEntry[];
  onShareSessionSelect: (targetUserID: string, session: TerminalEntry) => void;
}

function TeamMember(props: TeamMemberProps) {
  const [isSettingsModalOpened, setIsSettingsModalOpened] = useState(false);
  const [isShareShellSessionModalOpened, setIsShareShellSessionModalOpened] = useState(false);
  const [userID, setUserID] = useState('');


  useEffect(() => {
    const user = firebase.auth().currentUser;
    if (user) {
      setUserID(user.uid);
    }
  }, [firebase.auth().currentUser]);


  function closeShareShellSessionModal() {
    props.shellSessions.forEach(s => turnTerminalHighlightOff(s.terminalId));
    setIsShareShellSessionModalOpened(false);
  }

  function openShareShellSessionModal() {
    setIsShareShellSessionModalOpened(true);
  }

  function handleShareButtonClick() {
    if (isShareShellSessionModalOpened) { return; }
    openShareShellSessionModal();
  }

  function handleDotsClick() {
    if (isSettingsModalOpened) { return; }
    setIsSettingsModalOpened(true);
  }

  function handleCloseSettingsModal() {
    setIsSettingsModalOpened(false);
  }

  function handleShareShellSessionSelect(session: TerminalEntry) {
    props.onShareSessionSelect(props.member.userID, session);
    closeShareShellSessionModal();
  }

  return (
    <Container>
      {isSettingsModalOpened && (
        <UserSettingsModal onCloseModalRequest={handleCloseSettingsModal} />
      )}

      {isShareShellSessionModalOpened && (
        <ShareShellSessionModal
          onCloseModalRequest={closeShareShellSessionModal}
          member={props.member}
          shellSessions={props.shellSessions}
          onShareSessionSelect={handleShareShellSessionSelect}
        />
      )}
      <ActivityDot isOnline={props.member.isOnline} />
      <Username
        isOnline={props.member.isOnline}
      >
        {props.member.username}
        {userID === props.member.userID && (
          <YouTag>(you)</YouTag>
        )}
      </Username>

      <RightSideWrapper>
        {userID !== props.member.userID && props.member.isOnline && (
          <ShareTerminalButton
            onClick={handleShareButtonClick}
          >
            Share Terminal
          </ShareTerminalButton>
        )}

        {userID === props.member.userID && (
          <UserSettings
            onClick={handleDotsClick}
          />
        )}
      </RightSideWrapper>
    </Container>
  );
}

export default TeamMember;


