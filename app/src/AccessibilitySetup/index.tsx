import styled from 'styled-components';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import Loading from 'components/Loading';
import Button from 'components/Button';
import ErrorMessage from 'components/ErrorMessage';

import { isProcessTrusted } from '../mainProcess';

const Content = styled.div`
  padding: 10px;
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;


const Title = styled.span`
  margin-bottom: 30px;
  width: 100%;
  font-size: 25px;
  font-weight: 500;
  color: #fff;
  text-align: center;
`;

const Subtitle = styled(Title)`
  font-size: 20px;
`;

const Text = styled.span`
  margin-bottom: 20px;
  font-size: 15px;
  font-weight: 400;
  color: #fff;
`;

const UnderlineDiv = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column-reverse;
`;

const Underline = styled.span`
  padding-top: auto;
  margin-bottom: 15px;
  font-size: 12px;
  font-weight: 300;
  color: #fff;
`;

interface AccessibilitySetupProps {
  setIsAccessibilityAllowed: (value: boolean) => void;
  // onContinueClick: () => void;
}

function AccessibilitySetup(props: AccessibilitySetupProps) {
  const history = useHistory();

  const [isAccessibilityAllowed, setIsAccessibilityAllowed] = useState(false);

  useEffect(() => {
    async function check() {
      const accessibility = await isProcessTrusted();
      setIsAccessibilityAllowed(accessibility);
    }

    check();
  }, []);

  async function handleButtonClick() {
    if (!isAccessibilityAllowed) {
      const accessibility = await isProcessTrusted();
      console.log(accessibility);
      if (accessibility) {
        props.setIsAccessibilityAllowed(true);
        history.replace('/');
      }
    } else {
      props.setIsAccessibilityAllowed(true);
      history.replace('/');
    }
  }

  return (
    <Content>
      <Title>
        Sidekick
      </Title>

      <Subtitle>Sidekick needs accessibility permissions</Subtitle>

      <Text>1. Click on the "Open System Preferences" in the Accessibility Access popup menu</Text>
      <Text>2. Click on the lock icon in the left bottom corner to unlock the settings</Text>
      <Text>3. Click on the "sidekick" app in the list and allow accessibility</Text>
      <Button
        onClick={handleButtonClick}
      >
        CONTINUE
      </Button>
      <UnderlineDiv>
        <Underline>We need the accessibility permission so we can open new terminal windows when you share terminal sessions</Underline>
      </UnderlineDiv>
    </Content>
  );
}

export default AccessibilitySetup;
